// CODIGO DE ALEX!!!!!
/* Initial beliefs and rules */

//Comprueba que la posicion esta dentro del tablero.
noFueraTablero(Col,Fil) :- Col>=0 & Col<8 & Fil>=0 & Fil<8.

//Lo usamos para ver cuantas fichas tenemos juntas y determinar el movimiento optimo.
contador(1).

//Guardamos el valor m�ximo de movimiento.
maxValue(0).

//Guardamos el valor minimo del movimiento para perder
minimo(10).

//Movimiento optimo a hacer
movimientoOptimo(0,0).

//Sacamos el numero del jugador
numeroPlayer(1) :- .my_name(N) & N==player1.
numeroPlayer(2) :- .my_name(N) & N==player2.

//Sacamos el numero del oponente
numeroPlayerContrario(2) :- .my_name(N) & N==player1.
numeroPlayerContrario(1) :- .my_name(N) & N==player2.

//Miramos el jugador actual
jugadorActual(Player) :- .my_name(N) & Player=N.

/* Initial goals */
/* Plans */
//Si la estrategia la mando el entorno la deja
+estrategia(Actual)[source(percept)].

//Si la estrategia no la mando el entorno la eliminamos
+estrategia(Actual)<-
	.print("No eres el etorno esa no es la estrategia");
	-estrategia(Actual).

//Es su turno y juega a ganar
+turno(Player)[source(percept)]: jugadorActual(Player) & estrategia(jugarAPerder)<- 
	//Si cuela cuela, si no ahce comprobacion mueve dos veces seguidas.
	.send(player2,tell,turno(player2));
	.send(player2,untell,turno(player2));
	//Si cuela cuela, si no ahce comprobacionjuega a perder
	.send(player2,tell,estrategia(jugarAPerder));
	.send(player2,untell,estrategia(jugarAPerder));
	!noSeSiTengoFichas.
//Si aun no tiene fichas colocadas llama a noTengoFichas
+!noSeSiTengoFichas[source(self)]:numeroPlayer(N) & .findall([X,Y],tablero(X,Y,N),[])<-
	!noTengoFichas.
//Si ya tengo fichas colocadas llamamos a la estrategia.
+!noSeSiTengoFichas[source(self)]:numeroPlayer(N) & .findall([X,Y],tablero(X,Y,N),L)<-
	!tengoFichas(L).
	
//Al no tener fichas colocadas hace un findall te todos los espacios vacios y los guarda en L,, llamamos a mueve
+!noTengoFichas[source(self)]: .findall([X,Y],tablero(X,Y,0),L)<-
	!mueve(L).
//Del findall anterior saca la primera posicion y le dice al tablero cual ees para marcarla
+!mueve([[X,Y]|_])<-
	.wait(200);
	put(X,Y).

//Si tiene fichas construye la estrategia alrededor de ellas.
+!tengoFichas([[X,Y]|Cdr])[source(self)]<- 
	!comprobarAbajo(X,Y);
	!comprobarArriba(X,Y);
	!comprobarArribaDer(X,Y);
	!comprobarAbajoDer(X,Y);
	!comprobarIzquierda(X,Y);
	!comprobarDerecha(X,Y);
	!comprobarArribaIzq(X,Y);
	!comprobarAbajoIzq(X,Y);
	!tengoFichas(Cdr).
//Si llega aqui con lista vacia es porque hicimos todas las comprobaciones, asique hacemos el movimiento.
+!tengoFichas([])[source(self)]: movimientoOptimo(X,Y) & maxValue(M)<- 
	-maxValue(M);
	+maxValue(0);
	.wait(200);
	put(X,Y).
	
//COMPROBAMOS QUE TIENE A LA DERECHA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarDerecha(Col,Fil):numeroPlayerContrario(Num) & noFueraTablero(Col+1,Fil) & tablero(Col+1,Fil,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando a la derecha
+!comprobarDerecha(Col,Fil):numeroPlayer(Num) & noFueraTablero(Col+1,Fil) & tablero(Col+1,Fil,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarDerecha(Col+1,Fil).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarDerecha(Col,Fil): noFueraTablero(Col+1,Fil) & tablero(Col+1,Fil,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col+1,Fil);
	}.
//Si no esta en el tablero no hace nada
+!comprobarDerecha(Col,Fil)[source(self)]:contador(N)<-
	-contador(N);
	+contador(1).
	
//COMPROBAMOS QUE TIENE A LA IZQUIERDA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarIzquierda(Col,Fil):numeroPlayerContrario(Num) & noFueraTablero(Col-1,Fil) & tablero(Col-1,Fil,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando a la izquierda
+!comprobarIzquierda(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col-1,Fil) & tablero(Col-1,Fil,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarIzquierda(Col-1,Fil).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarIzquierda(Col,Fil): noFueraTablero(Col-1,Fil) & tablero(Col-1,Fil,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col-1,Fil);
	}.
//Si no esta en el tablero no hace nada
+!comprobarIzquierda(Col,Fil)[source(self)]: contador(N)<-
	-contador(N);
	+contador(1).
	
//COMPROBAMOS QUE TIENE ABAJO Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarAbajo(Col,Fil): numeroPlayerContrario(Num) & noFueraTablero(Col,Fil+1) & tablero(Col,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando para abajo
+!comprobarAbajo(Col,Fil):numeroPlayer(Num) & noFueraTablero(Col,Fil+1) & tablero(Col,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarAbajo(Col,Fil+1).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarAbajo(Col,Fil): noFueraTablero(Col,Fil+1) & tablero(Col,Fil+1,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col,Fil+1);
	}.
//Si no esta en el tablero no hace nada
+!comprobarAbajo(Col,Fil)[source(self)]: contador(N)<-
	-contador(N);
	+contador(1).
	
//COMPROBAMOS QUE TIENE ARRIBA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarArriba(Col,Fil): numeroPlayerContrario(Num) & noFueraTablero(Col,Fil-1) & tablero(Col,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando para arriba
+!comprobarArriba(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col,Fil-1) & tablero(Col,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarArriba(Col,Fil-1).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarArriba(Col,Fil): noFueraTablero(Col,Fil-1) & tablero(Col,Fil-1,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col,Fil-1);
	}.
//Si no esta en el tablero no hace nada
+!comprobarArriba(Col,Fil)[source(self)]:contador(N)<-
	-contador(N);
	+contador(1).	
	
//COMPROBAMOS QUE TIENE ARRIBA A LA IZQUIERDA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarArribaIzq(Col,Fil): numeroPlayerContrario(Num) & noFueraTablero(Col-1,Fil-1) & tablero(Col-1,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando arriba a la izquierda
+!comprobarArribaIzq(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col-1,Fil-1) & tablero(Col-1,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarArribaIzq(Col-1,Fil-1).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarArribaIzq(Col,Fil): noFueraTablero(Col-1,Fil-1) & tablero(Col-1,Fil-1,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col-1,Fil-1);
	}.
//Si no esta en el tablero no hace nada
+!comprobarArribaIzq(Col,Fil)[source(self)]:contador(N)<-
	-contador(N);
	+contador(1).
	
//COMPROBAMOS QUE TIENE ABAJO A LA IZQUIERDA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarAbajoIzq(Col,Fil): numeroPlayerContrario(Num) & noFueraTablero(Col-1,Fil+1) & tablero(Col-1,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando abajo a la izquierda
+!comprobarAbajoIzq(Col,Fil):numeroPlayer(Num) & noFueraTablero(Col-1,Fil+1) & tablero(Col-1,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarAbajoIzq(Col-1,Fil+1).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarAbajoIzq(Col,Fil): noFueraTablero(Col-1,Fil+1) & tablero(Col-1,Fil+1,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col-1,Fil+1);
	}.
//Si no esta en el tablero no hace nada
+!comprobarAbajoIzq(Col,Fil)[source(self)]:contador(N)<-
	-contador(N);
	+contador(1).
	
//COMPROBAMOS QUE TIENE ARRIBA A LA DERECHA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarArribaDer(Col,Fil): numeroPlayerContrario(Num) & noFueraTablero(Col+1,Fil-1) & tablero(Col+1,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando arriba a la derecha
+!comprobarArribaDer(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col+1,Fil-1) & tablero(Col+1,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarArribaDer(Col+1,Fil-1).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarArribaDer(Col,Fil): noFueraTablero(Col+1,Fil-1) & tablero(Col+1,Fil-1,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col+1,Fil-1);
	}.
//Si no esta en el tablero no hace nada
+!comprobarArribaDer(Col,Fil)[source(self)]:contador(N)<-
	-contador(N);
	+contador(1).
	
//COMPROBAMOS QUE TIENE ABAJO A LA DERECHA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha del otro no hace nada
+!comprobarAbajoDer(Col,Fil): numeroPlayerContrario(Num) & noFueraTablero(Col+1,Fil+1) & tablero(Col+1,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(1).
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando abajo a la derecha
+!comprobarAbajoDer(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col+1,Fil+1) & tablero(Col+1,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarAbajoDer(Col+1,Fil+1).
//Si sigue dentro del tablero y la posicion esta vacia.
//miramos si e contador es mayor o igual que el valorMaximo
//Si lo es lo cambiamos y anotamos el movimiento, sino no ahcemos nada
+!comprobarAbajoDer(Col,Fil): noFueraTablero(Col+1,Fil+1) & tablero(Col+1,Fil+1,0) & contador(Num) & maxValue(Max) & movimientoOptimo(ColOpt,FilOpt)& estrategia(jugarAGanar)<-
	-contador(Num);
	+contador(1);
	if(Num>=Max){
		-maxValue(Max);
		+maxValue(Num);
		-movimientoOptimo(ColOpt,FilOpt);
		+movimientoOptimo(Col+1,Fil+1);
	}.
//Si no esta en el tablero no hace nada
+!comprobarAbajoDer(Col,Fil)[source(self)]: contador(N)<-
	-contador(N);
	+contador(1).
	
//Es su turno y juega a perder
+turno(Player)[source(percept)]: jugadorActual(Player) & estrategia(jugarAGanar) & .findall([X,Y],tablero(X,Y,0),L)<- 
	.print("Es mi turno, y juego a perder");
	!estrategiaPerdedora(L).
	
//Comprueba que posicion junta menos fichas de su color y mueve ahi
+!estrategiaPerdedora([[X,Y]|Cdr])[source(self)]<- 
	!comprobarAbajoPerder(X,Y);
	!comprobarArribaPerder(X,Y);
	!esLaMejor(X,Y);
	!comprobarArribaIzqPerder(X,Y);
	!comprobarAbajoDerPerder(X,Y);
	!esLaMejor(X,Y);
	!comprobarArribaDerPerder(X,Y);
	!comprobarAbajoIzqPerder(X,Y);
	!esLaMejor(X,Y);
	!comprobarIzquierdaPerder(X,Y);
	!comprobarDerechaPerder(X,Y);
	!esLaMejor(X,Y);
	!compruebaSiOptima(X,Y);
	!estrategiaPerdedora(Cdr).

//Si llega aqui con lista vacia es porque hicimos todas las comprobaciones, asique hacemos el movimiento.
+!estrategiaPerdedora([])[source(self)]: movimientoOptimo(X,Y) & minimo(Min)<- 
	-minimo(Min);
	+minimo(10);
	.wait(200);
	put(X,Y).
	
//Miramos si la suma de fichas de nuestro color en estas direcciones es mayor que
//alguno de los movimientos de esta misma posicion en diferentes direcciones
//Si lo es lo ponemos como maximo valor a juntar de nuestra fichas si marcamos la posicion analizada
+!esLaMejor(X,Y)[source(self)]: contador(Mov) & maxValue(Max) & Mov>Max<-
	-contador(Mov);
	+contador(1);
	-maxValue(Max);
	+maxValue(Mov).
//Si no es mayor reseteamos el contador para las proximas direcciones
+!esLaMejor(X,Y)[source(self)]: contador(Mov)<-
	-contador(Mov);
	+contador(1).
	
//Miramos si la posicion actual junta menos fichas que la que teniamos marcada como minima.
//En ese caso la pondriamos como optima
+!compruebaSiOptima(X,Y)[source(self)]: maxValue(Max) & minimo(N) & Max<N & movimientoOptimo(ColOpt,FilOpt)<-	
	-minimo(N);
	+minimo(Max);
	-maxValue(Max);
	+maxValue(0);
	-movimientoOptimo(ColOpt,FilOpt);
	+movimientoOptimo(X,Y).
//Si no lo es reseteamos el valor maximo.
+!compruebaSiOptima(X,Y)[source(self)]: maxValue(Max)<-
	-maxValue(Max);
	+maxValue(0).

//COMPROBAMOS QUE TIENE A LA DERECHA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando a la derecha
+!comprobarDerechaPerder(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col+1,Fil) & tablero(Col+1,Fil,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarDerechaPerder(Col+1,Fil).
//Si no esta en el tablero no hace nada
+!comprobarDerechaPerder(Col,Fil)[source(self)].
	
//COMPROBAMOS QUE TIENE A LA IZQUIERDA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando a la izquierda
+!comprobarIzquierdaPerder(Col,Fil):numeroPlayer(Num) & noFueraTablero(Col-1,Fil) & tablero(Col-1,Fil,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarIzquierdaPerder(Col-1,Fil).
//Si no esta en el tablero no hace nada
+!comprobarIzquierdaPerder(Col,Fil)[source(self)].
	
//COMPROBAMOS QUE TIENE ABAJO Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando abajo
+!comprobarAbajoPerder(Col,Fil):numeroPlayer(Num) & noFueraTablero(Col,Fil+1) & tablero(Col,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarAbajoPerder(Col,Fil+1).
//Si no esta en el tablero no hace nada
+!comprobarAbajoPerder(Col,Fil)[source(self)].
	
//COMPROBAMOS QUE TIENE ARRIBA Y ACTUAMOS EN CONSECUENCIA
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando arriba
+!comprobarArribaPerder(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col,Fil-1) & tablero(Col,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarArribaPerder(Col,Fil-1).
//Si no esta en el tablero no hace nada
+!comprobarArribaPerder(Col,Fil)[source(self)].	
	
//COMPROBAMOS QUE TIENE ARRIBA A LA IZQUIERDA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando arriba a la izquierda
+!comprobarArribaIzqPerder(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col-1,Fil-1) & tablero(Col-1,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarArribaIzqPerder(Col-1,Fil-1).
//Si no esta en el tablero no hace nada
+!comprobarArribaIzqPerder(Col,Fil)[source(self)].
	
//COMPROBAMOS QUE TIENE ABAJO A LA IZQUIERDA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando abajo a la izquierda
+!comprobarAbajoIzqPerder(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col-1,Fil+1) & tablero(Col-1,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarAbajoIzqPerder(Col-1,Fil+1).
//Si no esta en el tablero no hace nada
+!comprobarAbajoIzqPerder(Col,Fil)[source(self)].
	
//COMPROBAMOS QUE TIENE ARRIBA A LA DERECHA Y ACTUAMOS EN CONSECUENCIA	
//Si sigue dentro del tablero y hay una ficha nuestra.
//Incrementamos el contador de movimiento optimo y seguimos comprobando arriba a la derecha
+!comprobarArribaDerPerder(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col+1,Fil-1) & tablero(Col+1,Fil-1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarArribaDerPerder(Col+1,Fil-1).
//Si no esta en el tablero no hace nada
+!comprobarArribaDerPerder(Col,Fil)[source(self)].
	
//COMPROBAMOS QUE TIENE ABAJO A LA DERECHA Y ACTUAMOS EN CONSECUENCIA	
//Incrementamos el contador de movimiento optimo y seguimos comprobando abajo a la derecha
+!comprobarAbajoDerPerder(Col,Fil): numeroPlayer(Num) & noFueraTablero(Col+1,Fil+1) & tablero(Col+1,Fil+1,Num) & contador(N)<-
	-contador(N);
	+contador(N+1);
	!comprobarAbajoDerPerder(Col+1,Fil+1).
+!comprobarAbajoDerPerder(Col,Fil)[source(self)].

+turno(Player): not jugadorActual(Player)
	<-  .print("No eres el entorno, no te hago caso").

/**
* Agente player.asl -> Agente capaz de jugar tanto a jugarAGanar como a jugarAPerder
* en un entorno dado por si mismo y sin intervención de ningún agente auxiliar
* Autores -> Adrián Araujo Gregorio & Cristhian Ferreiro Garrido
*/

// Todas las creencias necesarias son enviadas y actualizadas por el entorno.

/***************************************************
****************** Reglas **************************
***************************************************/

// NOTA: Todas las coordenadas están expresadas en formato X,Y, donde X representa 
// el número de columna e Y el número de fila.

/*********************************
***** Reglas de jugarAGanar******
*********************************/


/**
* Identifica el nombre del agente que está jugando actualmente.
* Player: Nombre del agente.
*/
jugadorActual(Player) 	:- .my_name(N) & Player = N.

/**
* Identifica las posiciones que no están ocupadas por ninguna ficha.
* L: Lista de posiciones libres del tablero. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
posicionLibre(L) :- .findall([X,Y],tablero(X,Y,0),L).

/**
* Es verdadero si el tablero se encuentra completamente vacío.
*/
tableroVacio :- posicionLibre(L) & tablero(T) & .length(T,NT) & .length(L,NL) & NT = NL.

/**
* Identifica las posiciones centrales del tablero para comenzar la partida controlando el centro.
* L: Lista de posiciones centrales. [[3,3],[3,4],[4,3],[4,4]].
*/
centro(L) :- L = [[3,3],[3,4],[4,3],[4,4]].

/**
* Identifica la lista de posiciones que se encuentran ocupadas por fichas del jugador.
* C: Lista de posiciones ocupadas por fichas del jugador. [[X1,Y1],[X2,Y2],...,[XN,YN]]..
*/
listarFichasColocadas(C) :- jugadorActual(player1) & .findall([X,Y],tablero(X,Y,1),C).
listarFichasColocadas(C) :- jugadorActual(player2) & .findall([X,Y],tablero(X,Y,2),C).

/**
* Selecciona una jugada aleatoria de entre las posibles opciones existentes.
* L: Lista de posibles jugadas que siguen la estrategía. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* X,Y: Coordenadas del tablero en las que se colocará la siguiente ficha.
*/
escogerMovimiento(L,X,Y) :- .shuffle(L,L2) & .member([X,Y],L2).

/**
* Cuando es el primer turno de juego, se coloca una ficha en las casillas centrales que se encuentren desocupadas.
* T: Lista de posiciones centrales que no se encuentran ocupadas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
listarMovimientos(T) :- listarFichasColocadas(F) & F=[] & posicionLibre(L) & centro(C) & .intersection(L,C,T).

/**
* Comprueba si se puede realizar algún 4 en raya en el tablero para ganar la partida.
* T: Lista de posiciones con las que puedes ganar la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
listarMovimientos(T) :- posicionLibre(L) & listarFichasColocadas(C) & colocarCuarta(H,C) & .intersection(L,H,T).

/**
* Comprueba si se pueden realizar uniones de 3 fichas en el tablero, para avanzar hacia la situación de victoria.
* T: Lista de posiciones en las que puedes alinear 3 fichas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
listarMovimientos(T) :- posicionLibre(L) & listarFichasColocadas(C) & huecosEntreFichas(H,C) & .intersection(L,H,T).

/**
* Busca una posición, a un hueco de distancia de una ficha del jugador, para realizar la estrategia ganadora.
* T: Lista de posiciones a un hueco de distancia de una ficha actual. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
listarMovimientos(T) :- posicionLibre(L) & listarFichasColocadas(C) & .member([X,Y],C) & posicionesCercanas(S,X,Y) & .intersection(L,S,T).

/**
* Situación excepcional, que no debería suceder en una partida, en la que se toma cualquier posición libre del tablero.
* L: Lista de posiciones desocupadas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
listarMovimientos(L) :- posicionLibre(L).

/**
* Identifica las posiciones que se encuentran a un hueco de distancia de otra ficha.
* L: Lista de posiciones a un hueco de distancia de una ficha actual. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
posicionesCercanas(L,X,Y) :- cercanasArriba(L1,X,Y) & cercanasAbajo(L2,X,Y) & cercanasDerecha(L3,X,Y) & cercanasIzquierda(L4,X,Y) & cercanasArribaDerecha(L5,X,Y) & cercanasAbajoDerecha(L6,X,Y) & cercanasArribaIzquierda(L7,X,Y) & cercanasAbajoIzquierda(L8,X,Y) & .concat(L1,L2,L3,L4,L5,L6,L7,L8,L).

/**
* Identifica las posiciones a un hueco de distancia hacia arriba.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasArriba([[X,Y-2]],X,Y) :- tablero(X,Y+1,0) & tablero(X,Y-1,0) & tablero(X,Y-2,0) & tablero(X,Y-3,0).
cercanasArriba([],X,Y).

/**
* Identifica las posiciones a un hueco de distancia hacia abajo.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasAbajo([[X,Y+2]],X,Y) :- tablero(X,Y-1,0) & tablero(X,Y+1,0) & tablero(X,Y+2,0) & tablero(X,Y+3,0).
cercanasAbajo([],X,Y).

/**
* Identifica las posiciones a un hueco de distancia hacia la derecha.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasDerecha([[X+2,Y]],X,Y) :- tablero(X-1,Y,0) & tablero(X+1,Y,0) & tablero(X+2,Y,0) & tablero(X+3,Y,0).
cercanasDerecha([],X,Y).

/**
* Identifica las posiciones a un hueco de distancia hacia la izquierda.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasIzquierda([[X-2,Y]],X,Y) :- tablero(X+1,Y,0) & tablero(X-1,Y,0) & tablero(X-2,Y,0) & tablero(X-3,Y,0).
cercanasIzquierda([],X,Y).

/**
* Identifica las posiciones a un hueco de distancia hacia arriba a la derecha.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasArribaDerecha([[X+2,Y-2]],X,Y) :- tablero(X-1,Y+1,0) & tablero(X+1,Y-1,0) & tablero(X+2,Y-2,0) & tablero(X+3,Y-3,0).
cercanasArribaDerecha([],X,Y).

/**
* Identifica las posiciones a un hueco de distancia hacia arriba a la izquierda.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasArribaIzquierda([[X-2,Y-2]],X,Y) :- tablero(X+1,Y+1,0) & tablero(X-1,Y-1,0) & tablero(X-2,Y-2,0) & tablero(X-3,Y-3,0).
cercanasArribaIzquierda([],X,Y).

/**
* Identifica las posiciones a un hueco de distancia hacia abajo a la derecha.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasAbajoDerecha([[X+2,Y+2]],X,Y) :- tablero(X-1,Y-1,0) & tablero(X+1,Y+1,0) & tablero(X+2,Y+2,0) & tablero(X+3,Y+3,0).
cercanasAbajoDerecha([],X,Y).

/**
* Identifica las posiciones a un hueco de distancia hacia abajo a la izquierda.
* [X,Y]: Coordenadas de la posición a un hueco de distancia que cumple las condiciones.
* X,Y: Coordenadas en el tablero de la ficha sobre la que se comprueba.
*/
cercanasAbajoIzquierda([[X-2,Y+2]],X,Y) :- tablero(X+1,Y-1,0) & tablero(X-1,Y+1,0) & tablero(X-2,Y+2,0) & tablero(X-3,Y+3,0).
cercanasAbajoIzquierda([],X,Y).

/**
* En Modo Perder identifica las posiciones en las que se realiza un 4 en raya y se pierde la partida.
* L: Lista de posiciones en las que se pierde la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* C: Lista de posiciones en las que se encuentran las fichas del jugador. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
perderPartida(L,C) :- perderHorizontal(L1,C) & perderVertical(L2,C) & perderAscendente(L3,C) & perderDescendente(L4,C) & .concat(L1,L2,L3,L4,L).

/**
* Comprueba los posibles 4 en raya en horizontal.
* L: Lista de posiciones en las que se pierde la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,P) & tablero(X-1,Y,0) & perderHorizontal(LF,Tail) & .concat([[X-1,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,0) & tablero(X+2,Y,P) & tablero(X+3,Y,P) & perderHorizontal(LF,Tail) & .concat([[X+1,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,0) & tablero(X+3,Y,P) & perderHorizontal(LF,Tail) & .concat([[X+2,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,P) & tablero(X+3,Y,0) & perderHorizontal(LF,Tail) & .concat([[X+3,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- perderHorizontal(L,Tail).
perderHorizontal([],[]).

/**
* Comprueba los posibles 4 en raya en vertical.
* L: Lista de posiciones en las que se pierde la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,P) & tablero(X,Y-1,0) & perderVertical(LF,Tail) & .concat([[X,Y-1]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,0) & tablero(X,Y+2,P) & tablero(X,Y+3,P) & perderVertical(LF,Tail) & .concat([[X,Y+1]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,0) & tablero(X,Y+3,P) & perderVertical(LF,Tail) & .concat([[X,Y+2]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,P) & tablero(X,Y+3,0) & perderVertical(LF,Tail) & .concat([[X,Y+3]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- perderVertical(L,Tail).
perderVertical([],[]).

/**
* Comprueba los posibles 4 en raya en la diagonal descendente.
* L: Lista de posiciones en las que se pierde la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,P) & tablero(X-1,Y-1,0) & perderDescendente(LF,Tail) & .concat([[X-1,Y-1]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,0) & tablero(X+2,Y+2,P) & tablero(X+3,Y+3,P) & perderDescendente(LF,Tail) & .concat([[X+1,Y+1]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,0) & tablero(X+3,Y+3,P) & perderDescendente(LF,Tail) & .concat([[X+2,Y+2]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,P) & tablero(X+3,Y+3,0) & perderDescendente(LF,Tail) & .concat([[X+3,Y+3]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- perderDescendente(L,Tail).
perderDescendente([],[]).

/**
* Comprueba los posibles 4 en raya en la diagonal ascendente.
* L: Lista de posiciones en las que se pierde la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,P) & tablero(X-1,Y+1,0) & perderAscendente(LF,Tail) & .concat([[X-1,Y+1]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,0) & tablero(X+2,Y-2,P) & tablero(X+3,Y-3,P) & perderAscendente(LF,Tail) & .concat([[X+1,Y-1]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,0) & tablero(X+3,Y-3,P) & perderAscendente(LF,Tail) & .concat([[X+2,Y-2]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,P) & tablero(X+3,Y-3,0) & perderAscendente(LF,Tail) & .concat([[X+3,Y-3]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- perderAscendente(L,Tail).
perderAscendente([],[]).

/**
* En Modo Ganar identifica las posiciones en las que se realiza un 4 en raya y se gana la partida.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* C: Lista de posiciones en las que se encuentran las fichas del jugador. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarCuarta(L,C) :- colocarArriba(L1,C) & colocarDerecha(L2,C) & colocarArribaDerecha(L3,C) & colocarAbajoDerecha(L4,C) & colocarAbajo(L5,C) & colocarIzquierda(L6,C) & colocarAbajoIzquierda(L7,C) & colocarArribaIzquierda(L8,C) & .concat(L1,L2,L3,L4,L5,L6,L7,L8,L).

/**
* Comprueba los posibles 4 en raya hacia arriba.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarArriba(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y-1,P) & tablero(X,Y-2,P) & tablero(X,Y-3,0) & colocarArriba(LF,Tail) & .concat([[X,Y-3]],LF,L).
colocarArriba(L,[[X,Y]|Tail]) :- colocarArriba(L,Tail).
colocarArriba([],[]).

/**
* Comprueba los posibles 4 en raya hacia abajo.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarAbajo(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,P) & tablero(X,Y+3,0) & colocarAbajo(LF,Tail) & .concat([[X,Y+3]],LF,L).
colocarAbajo(L,[[X,Y]|Tail]) :- colocarAbajo(L,Tail).
colocarAbajo([],[]).

/**
* Comprueba los posibles 4 en raya hacia la derecha.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,P) & tablero(X+3,Y,0) & colocarDerecha(LF,Tail) & .concat([[X+3,Y]],LF,L).
colocarDerecha(L,[[X,Y]|Tail]) :- colocarDerecha(L,Tail).
colocarDerecha([],[]).

/**
* Comprueba los posibles 4 en raya hacia la izquierda.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarIzquierda(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X-1,Y,P) & tablero(X-2,Y,P) & tablero(X-3,Y,0) & colocarIzquierda(LF,Tail) & .concat([[X-3,Y]],LF,L).
colocarIzquierda(L,[[X,Y]|Tail]) :- colocarIzquierda(L,Tail).
colocarIzquierda([],[]).
     
/**
* Comprueba los posibles 4 en raya hacia arriba a la derecha.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarArribaDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,P) & tablero(X+3,Y-3,0) & colocarArribaDerecha(LF,Tail) & .concat([[X+3,Y-3]],LF,L).
colocarArribaDerecha(L,[[X,Y]|Tail]) :- colocarArribaDerecha(L,Tail).
colocarArribaDerecha([],[]).

/**
* Comprueba los posibles 4 en raya hacia abajo a la izquierda.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarAbajoIzquierda(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X-1,Y+1,P) & tablero(X-2,Y+2,P) & tablero(X-3,Y+3,0) & colocarAbajoIzquierda(LF,Tail) & .concat([[X-3,Y+3]],LF,L).
colocarAbajoIzquierda(L,[[X,Y]|Tail]) :- colocarAbajoIzquierda(L,Tail).
colocarAbajoIzquierda([],[]).

/**
* Comprueba los posibles 4 en raya hacia abajo a la derecha.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarAbajoDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,P) & tablero(X+3,Y+3,0) & colocarAbajoDerecha(LF,Tail) & .concat([[X+3,Y+3]],LF,L).
colocarAbajoDerecha(L,[[X,Y]|Tail]) :- colocarAbajoDerecha(L,Tail).
colocarAbajoDerecha([],[]).

/**
* Comprueba los posibles 4 en raya hacia arriba a la izquierda.
* L: Lista de posiciones en las que se gana la partida. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
colocarArribaIzquierda(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X-1,Y-1,P) & tablero(X-2,Y-2,P) & tablero(X-3,Y-3,0) & colocarArribaIzquierda(LF,Tail) & .concat([[X-3,Y-3]],LF,L).
colocarArribaIzquierda(L,[[X,Y]|Tail]) :- colocarArribaIzquierda(L,Tail).
colocarArribaIzquierda([],[]).

/**
* Identifica las posiciones vacías que se encuentran entre dos fichas del jugador.
* L: Lista de posiciones en las que se coloca entre dos fichas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* C: Lista de posiciones en las que se encuentran las fichas del jugador. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
huecosEntreFichas(L,C) :- huecoArriba(L1,C) & huecoDerecha(L2,C) & huecoArribaDerecha(L3,C) & huecoAbajoDerecha(L4,C) & .concat(L1,L2,L3,L4,L).

/**
* Comprueba las posiciones en vertical.
* L: Lista de posiciones en las que se coloca entre dos fichas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
huecoArriba(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y-1,0) & tablero(X,Y-2,P) & huecoArriba(LF,Tail) & .concat([[X,Y-1]],LF,L).
huecoArriba(L,[[X,Y]|Tail]) :- huecoArriba(L,Tail).
huecoArriba([],[]).

/**
* Comprueba las posiciones en horizontal.
* L: Lista de posiciones en las que se coloca entre dos fichas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
huecoDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,0) & tablero(X+2,Y,P) & huecoDerecha(LF,Tail) & .concat([[X+1,Y]],LF,L).
huecoDerecha(L,[[X,Y]|Tail]) :- huecoDerecha(L,Tail).
huecoDerecha([],[]).

/**
* Comprueba las posiciones en la diagonal ascendente.
* L: Lista de posiciones en las que se coloca entre dos fichas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
huecoArribaDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,0) & tablero(X+2,Y-2,P) & huecoArribaDerecha(LF,Tail) & .concat([[X+1,Y-1]],LF,L).
huecoArribaDerecha(L,[[X,Y]|Tail]) :- huecoArribaDerecha(L,Tail).
huecoArribaDerecha([],[]).

/**
* Comprueba las posiciones en la diagonal descendente.
* L: Lista de posiciones en las que se coloca entre dos fichas. [[X1,Y1],[X2,Y2],...,[XN,YN]].
* @param2: Lista de coordenadas de las fichas del jugador sobre las que se realiza la comprobación. [[X1,Y1],[X2,Y2],...,[XN,YN]].
*/
huecoAbajoDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,0) & tablero(X+2,Y+2,P) & huecoAbajoDerecha(LF,Tail) & .concat([[X+1,Y+1]],LF,L).
huecoAbajoDerecha(L,[[X,Y]|Tail]) :- huecoAbajoDerecha(L,Tail).
huecoAbajoDerecha([],[]).

/*********************************
***** Reglas de jugarAPerder******
*********************************/

/**
* Calcula la distancia entre dos puntos dados con la fórmula D=?((X2-X1)^2+(Y2-Y1)^2)) 
* X1: numero de columna del punto 1.
* Y1: numero de fila del punto 1.
* X2: numero de columna del punto 2.
* Y1: numero de fila del punto 2.
*/
distancia(X1,Y1,X2,Y2,D) :- D = math.sqrt(((X2-X1)*(X2-X1))+((Y2-Y1)*(Y2-Y1))).

/**
* Genera una lista con todas las casillas del tablero que estén ocupadas
* C: lista de estructura [[X1,Y1],[X2,Y2][XN,YN]] que contiene todas las casillas ocupadas del tablero.
*/
listarCasillasOcupadas(C) :- .findall([X,Y],tablero(X,Y,1),C1) & .findall([X,Y],tablero(X,Y,2),C2) & .concat(C1,C2,C).

/**
* Escoge una posición aleatoria de una lista dada que no haga cuatro en raya
* L: lista de estructura [[X1,Y1],[X2,Y2][XN,YN]] con todas las posiciones disponibles para escoger.
* X: numero de columna de la casilla escogida.
* Y: numero de fila de la casilla escogida.
*/
jugadaAleatoriaCorrecta(L,X,Y) :- 
	listarFichasColocadas(C) &
	perderPartida(R,C) &
	.difference(L,R,P) &
	escogerMovimiento(P,X,Y).

/**
* Genera una lista con la distancia de cada casilla de una lista dada a una casilla de referencia
* Ref: punto de referencia de forma [X1,Y2]
* @param2: lista de estructura [[X1,Y1],[X2,Y2][XN,YN]] con los puntos desde los que se quiere calcular la distancia con Ref.
* L: lista de estructura [[X1,Y1,D1],[X2,Y2,D2][XN,YN,DN]] donde se va concatenando en cada iteración el punto y su distancia a Ref.
* Res: lista de estructura [[X1,Y1,D1],[X2,Y2,D2][XN,YN,DN]] que contiene la distancia de todos los elementos de param2
*/
listaDistancias(Ref,[],L,Res) :- L = Res.
listaDistancias([X1,Y1],[[X2,Y2]|Tail],L,Res) :- 
	distancia(X1,Y1,X2,Y2,D) &
	.concat(L,[[X1,Y1,D]],NL) &
	listaDistancias([X1,Y1],Tail,NL,Res).
	
/**
Dada una lista de coordenadas y distancias a una casilla de referencias, calcula cual de ellas está a una mayor distancia.
* @param1: lista de estructura [[X1,Y1,D1],[X2,Y2,D2][XN,YN,DN]]
* Temp: vector auxiliar de estructura [XN,YN,DN] con el punto con mayor distancia encontrado hasta el momento.
* Res: vector de estructura [XN,YN,DN] que contiene el punto con mayor distancia de los contenidos en la lista de @param1
*/
maxDistancia([],Temp,Res) :- Res = Temp.
maxDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :- 
	D > DT & 
	listarFichasColocadas(C) &
	maxDistancia(Tail,[X,Y,D],Res).
maxDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :- 
	maxDistancia(Tail,[XT,YT,DT],Res).

/**
Dada una lista de coordenadas y distancias a una casilla de referencias, calcula cual de ellas está a una menor distancia.
* @param1: lista de estructura [[X1,Y1,D1],[X2,Y2,D2][XN,YN,DN]]
* Temp: vector auxiliar de estructura [XN,YN,DN] con el punto a menor distancia encontrado hasta el momento.
* Res: vector de estructura [XN,YN,DN] que contiene el punto con menor distancia de los contenidos en la lista de @param1
*/
minDistancia([],Temp,Res) :- 
	Res = Temp.
minDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :- 
	D < DT & 
	listarFichasColocadas(C) &
	minDistancia(Tail,[X,Y,D],Res).
minDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :-
	minDistancia(Tail,[XT,YT,DT],Res).

/**
* Genera una lista con todas las casillas disponibles para el próximo movimiento y su distancia a la casilla ocupada más cercana.
* @param1: lista de estructura [[X1,Y1],[X2,Y2][XN,YN]] que contiene las casillas disponibles para llevar a cabo el próximo movimiento.
* O: lista de estructura [[X1,Y1],[X2,Y2][XN,YN]] que contiene las casillas ocupadas del tablero.
* Temp: lista auxiliar de esctuctura [[X1,Y1,D1],[X2,Y2,D2][XN,YN,DN]] donde se van concatenando en cada iteración cada casilla
* 	   disponible y su correspondiente distancia a la casilla ocupada más cercana.
* Res: lista de estructura [[X1,Y1,D1],[X2,Y2,D2][XN,YN,DN]] que contiene la distancia a la casilla ocupada más cercana para todas
*     las casillas de @param1.
*/
posiblesJugadasPerder([],O,Temp,Res) :- Temp = Res.
posiblesJugadasPerder([[X,Y]|Tail],O,Temp,Res) :- 
	listaDistancias([X,Y],O,[],LD) &
	posicionLibre(L) &
	jugadaAleatoriaCorrecta(L,X1,Y1) &
	minDistancia(LD,[X1,Y1,20],MD) &		// Busca una posición disponible aleatoria para inicializar la variable auziliar (si la lista de distancias está vacia, será la variable escogida).
	.concat(Temp,[MD],TempRes) &
	posiblesJugadasPerder(Tail,O,TempRes,Res).
	
/**
* Escoge la casilla óptima para colocar una ficha y evitar hacer cuatro en raya, esto es, la casilla disponible que tenga la casilla
* ocupada más cercana más lejos.
* L: lista de estructura [[X1,Y1],[X2,Y2][XN,YN]] que contiene las casillas disponibles para llevar a cabo el próximo movimiento.
* En caso de que L esté vacío (todas las casillas libres hacen cuatro-en-raya, escoge una casilla aleatoria entre las que estén
* libres.
* O: lista de estructura [[X1,Y1],[X2,Y2][XN,YN]] que contiene las casillas ocupadas del tablero.
* X: número de columna de la casilla considerada como óptima.
* Y: numero de fila de la casilla considerada como óptima.
*/
casillaOptimaPerder([],O,X,Y) :- posicionLibre(L) & escogerMovimiento(L,X,Y). 
casillaOptimaPerder(L,O,X,Y) :- 
	posiblesJugadasPerder(L,O,[],Res) &
	jugadaAleatoriaCorrecta(L,X1,Y1) &		// Busca una posición disponible aleatoria para inicializar la variable auxiliar (si la lista de posibles jugadas está vacía, será la escogida).
	maxDistancia(Res,[X1,Y1,0],[X,Y,D]).

/**
* Identifica las esquinas del tablero.
* L: Lista de posiciones extremas. [[0,0],[7,7],[0,7],[7,0]].
*/
extremos(L) :- L = [[0,0],[7,7],[0,7],[7,0]].

/**
* Escoge la casilla para realizar el siguiente movimiento dentro de la estrategia jugarAPerder. Esto es, si es posible,
* una de las casillas de las esquinas, si no,la ficha seleccionada por la regla casillaOptimaPerder.
* X: número de columna de la casilla en la que se colocará ficha.
* Y: numero de fila de la casilla en la que se colocará ficha.
*/
escogerMovimientoPerder(X,Y) :- 
	extremos(E) &
	posicionLibre(L) & 
	.intersection(L,E,R) &
	escogerMovimiento(R,X,Y). //tableroVacio.
escogerMovimientoPerder(X,Y) :- 
	posicionLibre(L) & 
	listarFichasColocadas(O) &	// Solo se consideran las fichas propias
	perderPartida(Perder,O) &
	.difference(L,Perder,L2) &	// Se excluyen de las casillas disponibles aquellas que llevan a un cuatro en raya inmediato.
	casillaOptimaPerder(L2,O,X,Y).

/*************************************************
******** Ejecución de la estrategia **************
*************************************************/

// Es mi turno en modo jugarAGanar
+turno(Player)[source(percept)]: jugadorActual(Player) & estrategia(Estrategia)[source(percept)] & Estrategia = jugarAGanar <-
	.print("TURNO: ", Player);
	.wait(500);						// Espera para asegurarse de que la sincronización con el tablero es correcta
	!escogerMovimientoGanar.

// Es mi turno en modo jugarAPerder
+turno(Player)[source(percept)]: jugadorActual(Player) & estrategia(Estrategia)[source(percept)] & Estrategia = jugarAPerder <-
	.print("TURNO: ", Player);
	.wait(500);						// Espera para asegurarse de que la sincronización con el tablero es correcta
	!escogerMovimientoPerder.

+!escogerMovimientoGanar: listarMovimientos(L) & escogerMovimiento(L,X,Y) <-
	.print(X," ",Y);
	put(X,Y).
	
+!escogerMovimientoGanar <- .print("MOVIMIENTO INVALIDO").

+!escogerMovimientoPerder: escogerMovimientoPerder(X,Y) <- 
	.print(X," ",Y);
	put(X,Y).
	
+!escogerMovimientoPerder <- .print("MOVIMIENTO INVALIDO"). 

/***************************************************
************* CONTROL DE TRAMPAS *******************
***************************************************/

// Han intentado engañarle con respecto al turno
+turno(Player)[source(A)]: not A = percept <- 
	.print("Alguien ha tratado de hacer trampa!").
+turno(Player).

// Han intentado manipular la estrategia
+estrategia(Estrategia)[source(A)] : not A = percept <-
	-estrategia(Estrategia)[source(A)];					// Borra la falsa creencia
	.print("Alguien ha tratado de hacer trampa!").
+estrategia(Estrategia).

// Han intentado manipular su percepción del tablero
+tablero(E)[source(A)] : not A = percept <-
	-tablero(E)[source(A)];								// Borra la falsa creencia
	.print("Alguien ha tratado de hacer trampa!").
+tablero(E).

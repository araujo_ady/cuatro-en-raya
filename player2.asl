

//Devuelve el nombre del jugador actual.
jugadorActual(Player) 	:- .my_name(N) & Player = N.

//Devuelve las posiciones que no est�n ocupadas.
posicionLibre(L) :- .findall([X,Y],tablero(X,Y,0),L).

// False si hay ya alguna ficha colocada en el tablero
tableroVacio :- posicionLibre(L) & tablero(T) & .length(T,NT) & .length(L,NL) & NT = NL.

//Devuelve las posiciones centrales del tablero.
centro(L) :- L = [[3,3],[3,4],[4,3],[4,4]].

//Devuelve la lista de fichas que has colocado.
listarFichasColocadas(C) :- jugadorActual(player1) & .findall([X,Y],tablero(X,Y,1),C).
listarFichasColocadas(C) :- jugadorActual(player2) & .findall([X,Y],tablero(X,Y,2),C).



//Escoge una posicion aleatoria de la lista de posibles jugadas.
escogerMovimiento(L,X,Y) :- .shuffle(L,L2) & .member([X,Y],L2).



//En el primer turno coloca una ficha en las casillas centrales.
listarMovimientos(T) :- listarFichasColocadas(F) & F=[] & posicionLibre(L) & centro(C) & .intersection(L,C,T).

//Realiza el 4 en Raya.
listarMovimientos(T) :- posicionLibre(L) & listarFichasColocadas(C) & colocarCuarta(H,C) & .intersection(L,H,T).

//Realiza uniones de 3 fichas.
listarMovimientos(T) :- posicionLibre(L) & listarFichasColocadas(C) & huecosEntreFichas(H,C) & .intersection(L,H,T).

//Coloca la siguiente ficha a un hueco de distancia de una actual.
listarMovimientos(T) :- posicionLibre(L) & listarFichasColocadas(C) & .member([X,Y],C) & posicionesCercanas(S,X,Y) & .intersection(L,S,T).

//�Excepcionalmente!, coloca aleatoriamente.
listarMovimientos(L) :- posicionLibre(L).



//Devuelve las posiciones a un hueco de distancia de una ficha.
posicionesCercanas(L,X,Y) :- cercanasArriba(L1,X,Y) & cercanasAbajo(L2,X,Y) & cercanasDerecha(L3,X,Y) & cercanasIzquierda(L4,X,Y) & cercanasArribaDerecha(L5,X,Y) & cercanasAbajoDerecha(L6,X,Y) & cercanasArribaIzquierda(L7,X,Y) & cercanasAbajoIzquierda(L8,X,Y) & .concat(L1,L2,L3,L4,L5,L6,L7,L8,L).

//Devuelve las posiciones a un hueco de distancia hacia arriba.
cercanasArriba([[X,Y-2]],X,Y) :- tablero(X,Y+1,0) & tablero(X,Y-1,0) & tablero(X,Y-2,0) & tablero(X,Y-3,0).
cercanasArriba([],X,Y).

//Devuelve las posiciones a un hueco de distancia hacia abajo.
cercanasAbajo([[X,Y+2]],X,Y) :- tablero(X,Y-1,0) & tablero(X,Y+1,0) & tablero(X,Y+2,0) & tablero(X,Y+3,0).
cercanasAbajo([],X,Y).

//Devuelve las posiciones a un hueco de distancia hacia la derecha.
cercanasDerecha([[X+2,Y]],X,Y) :- tablero(X-1,Y,0) & tablero(X+1,Y,0) & tablero(X+2,Y,0) & tablero(X+3,Y,0).
cercanasDerecha([],X,Y).

//Devuelve las posiciones a un hueco de distancia hacia la izquierda.
cercanasIzquierda([[X-2,Y]],X,Y) :- tablero(X+1,Y,0) & tablero(X-1,Y,0) & tablero(X-2,Y,0) & tablero(X-3,Y,0).
cercanasIzquierda([],X,Y).

//Devuelve las posiciones a un hueco de distancia hacia arriba derecha.
cercanasArribaDerecha([[X+2,Y-2]],X,Y) :- tablero(X-1,Y+1,0) & tablero(X+1,Y-1,0) & tablero(X+2,Y-2,0) & tablero(X+3,Y-3,0).
cercanasArribaDerecha([],X,Y).

//Devuelve las posiciones a un hueco de distancia hacia arriba izquierda.
cercanasArribaIzquierda([[X-2,Y-2]],X,Y) :- tablero(X+1,Y+1,0) & tablero(X-1,Y-1,0) & tablero(X-2,Y-2,0) & tablero(X-3,Y-3,0).
cercanasArribaIzquierda([],X,Y).

//Devuelve las posiciones a un hueco de distancia hacia abajo derecha.
cercanasAbajoDerecha([[X+2,Y+2]],X,Y) :- tablero(X-1,Y-1,0) & tablero(X+1,Y+1,0) & tablero(X+2,Y+2,0) & tablero(X+3,Y+3,0).
cercanasAbajoDerecha([],X,Y).

//Devuelve las posiciones a un hueco de distancia hacia abajo izquierda.
cercanasAbajoIzquierda([[X-2,Y+2]],X,Y) :- tablero(X+1,Y-1,0) & tablero(X-1,Y+1,0) & tablero(X-2,Y+2,0) & tablero(X-3,Y+3,0).
cercanasAbajoIzquierda([],X,Y).

//Devuelve la posici�n en la que pierdes la partida.
perderPartida(L,C) :- perderHorizontal(L1,C) & .print("HOR: ",L1) & perderVertical(L2,C) & .print("VER: ",L2) & perderAscendente(L3,C) & .print("ASC: ",L3) & perderDescendente(L4,C) & .print("DES: ",L4) & .concat(L1,L2,L3,L4,L).

perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,P) & tablero(X-1,Y,0) & perderHorizontal(LF,Tail) & .concat([[X-1,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,0) & tablero(X+2,Y,P) & tablero(X+3,Y,P) & perderHorizontal(LF,Tail) & .concat([[X+1,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,0) & tablero(X+3,Y,P) & perderHorizontal(LF,Tail) & .concat([[X+2,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,P) & tablero(X+3,Y,0) & perderHorizontal(LF,Tail) & .concat([[X+3,Y]],LF,L).
perderHorizontal(L,[[X,Y]|Tail]) :- perderHorizontal(L,Tail).
perderHorizontal([],[]).

perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,P) & tablero(X,Y-1,0) & perderVertical(LF,Tail) & .concat([[X,Y-1]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,0) & tablero(X,Y+2,P) & tablero(X,Y+3,P) & perderVertical(LF,Tail) & .concat([[X,Y+1]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,0) & tablero(X,Y+3,P) & perderVertical(LF,Tail) & .concat([[X,Y+2]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,P) & tablero(X,Y+3,0) & perderVertical(LF,Tail) & .concat([[X,Y+3]],LF,L).
perderVertical(L,[[X,Y]|Tail]) :- perderVertical(L,Tail).
perderVertical([],[]).

perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,P) & tablero(X-1,Y-1,0) & perderDescendente(LF,Tail) & .concat([[X-1,Y-1]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,0) & tablero(X+2,Y+2,P) & tablero(X+3,Y+3,P) & perderDescendente(LF,Tail) & .concat([[X+1,Y+1]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,0) & tablero(X+3,Y+3,P) & perderDescendente(LF,Tail) & .concat([[X+2,Y+2]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,P) & tablero(X+3,Y+3,0) & perderDescendente(LF,Tail) & .concat([[X+3,Y+3]],LF,L).
perderDescendente(L,[[X,Y]|Tail]) :- perderDescendente(L,Tail).
perderDescendente([],[]).

perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,P) & tablero(X-1,Y+1,0) & perderAscendente(LF,Tail) & .concat([[X-1,Y+1]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,0) & tablero(X+2,Y-2,P) & tablero(X+3,Y-3,P) & perderAscendente(LF,Tail) & .concat([[X+1,Y-1]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,0) & tablero(X+3,Y-3,P) & perderAscendente(LF,Tail) & .concat([[X+2,Y-2]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,P) & tablero(X+3,Y-3,0) & perderAscendente(LF,Tail) & .concat([[X+3,Y-3]],LF,L).
perderAscendente(L,[[X,Y]|Tail]) :- perderAscendente(L,Tail).
perderAscendente([],[]).


//Devuelve la posici�n en la que puedes ganar la partida.
colocarCuarta(L,C) :- colocarArriba(L1,C) & colocarDerecha(L2,C) & colocarArribaDerecha(L3,C) & colocarAbajoDerecha(L4,C) & colocarAbajo(L5,C) & colocarIzquierda(L6,C) & colocarAbajoIzquierda(L7,C) & colocarArribaIzquierda(L8,C) & .concat(L1,L2,L3,L4,L5,L6,L7,L8,L).

//Devuelve las posiciones de victoria hacia arriba.
colocarArriba(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y-1,P) & tablero(X,Y-2,P) & tablero(X,Y-3,0) & colocarArriba(LF,Tail) & .concat([[X,Y-3]],LF,L).
colocarArriba(L,[[X,Y]|Tail]) :- colocarArriba(L,Tail).
colocarArriba([],[]).

//Devuelve las posiciones de victoria hacia abajo.
colocarAbajo(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y+1,P) & tablero(X,Y+2,P) & tablero(X,Y+3,0) & colocarAbajo(LF,Tail) & .concat([[X,Y+3]],LF,L).
colocarAbajo(L,[[X,Y]|Tail]) :- colocarAbajo(L,Tail).
colocarAbajo([],[]).

//Devuelve las posiciones de victoria hacia la derecha.
colocarDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,P) & tablero(X+2,Y,P) & tablero(X+3,Y,0) & colocarDerecha(LF,Tail) & .concat([[X+3,Y]],LF,L).
colocarDerecha(L,[[X,Y]|Tail]) :- colocarDerecha(L,Tail).
colocarDerecha([],[]).

//Devuelve las posiciones de victoria hacia la izquierda.
colocarIzquierda(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X-1,Y,P) & tablero(X-2,Y,P) & tablero(X-3,Y,0) & colocarIzquierda(LF,Tail) & .concat([[X-3,Y]],LF,L).
colocarIzquierda(L,[[X,Y]|Tail]) :- colocarIzquierda(L,Tail).
colocarIzquierda([],[]).

//Devuelve las posiciones de victoria hacia arriba derecha.
colocarArribaDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,P) & tablero(X+2,Y-2,P) & tablero(X+3,Y-3,0) & colocarArribaDerecha(LF,Tail) & .concat([[X+3,Y-3]],LF,L).
colocarArribaDerecha(L,[[X,Y]|Tail]) :- colocarArribaDerecha(L,Tail).
colocarArribaDerecha([],[]).

//Devuelve las posiciones de victoria hacia abajo izquierda.
colocarAbajoIzquierda(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X-1,Y+1,P) & tablero(X-2,Y+2,P) & tablero(X-3,Y+3,0) & colocarAbajoIzquierda(LF,Tail) & .concat([[X-3,Y+3]],LF,L).
colocarAbajoIzquierda(L,[[X,Y]|Tail]) :- colocarAbajoIzquierda(L,Tail).
colocarAbajoIzquierda([],[]).

//Devuelve las posiciones de victoria hacia abajo derecha.
colocarAbajoDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,P) & tablero(X+2,Y+2,P) & tablero(X+3,Y+3,0) & colocarAbajoDerecha(LF,Tail) & .concat([[X+3,Y+3]],LF,L).
colocarAbajoDerecha(L,[[X,Y]|Tail]) :- colocarAbajoDerecha(L,Tail).
colocarAbajoDerecha([],[]).

//Devuelve las posiciones de victoria hacia arriba izquierda.
colocarArribaIzquierda(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X-1,Y-1,P) & tablero(X-2,Y-2,P) & tablero(X-3,Y-3,0) & colocarArribaIzquierda(LF,Tail) & .concat([[X-3,Y-3]],LF,L).
colocarArribaIzquierda(L,[[X,Y]|Tail]) :- colocarArribaIzquierda(L,Tail).
colocarArribaIzquierda([],[]).



//Devuelve las posiciones de las fichas que se encuentran entre otras dos fichas.
huecosEntreFichas(L,C) :- huecoArriba(L1,C) & huecoDerecha(L2,C) & huecoArribaDerecha(L3,C) & huecoAbajoDerecha(L4,C) & .concat(L1,L2,L3,L4,L).

//Devuelve las posiciones que se encuentran entre otras dos fichas de forma vertical.
huecoArriba(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X,Y-1,0) & tablero(X,Y-2,P) & huecoArriba(LF,Tail) & .concat([[X,Y-1]],LF,L).
huecoArriba(L,[[X,Y]|Tail]) :- huecoArriba(L,Tail).
huecoArriba([],[]).

//Devuelve las posiciones que se encuentran entre otras dos fichas de forma horizontal.
huecoDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y,0) & tablero(X+2,Y,P) & huecoDerecha(LF,Tail) & .concat([[X+1,Y]],LF,L).
huecoDerecha(L,[[X,Y]|Tail]) :- huecoDerecha(L,Tail).
huecoDerecha([],[]).

//Devuelve las posiciones que se encuentran entre otras dos fichas de forma diagonal ascendente.
huecoArribaDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y-1,0) & tablero(X+2,Y-2,P) & huecoArribaDerecha(LF,Tail) & .concat([[X+1,Y-1]],LF,L).
huecoArribaDerecha(L,[[X,Y]|Tail]) :- huecoArribaDerecha(L,Tail).
huecoArribaDerecha([],[]).

//Devuelve las posiciones que se encuentran entre otras dos fichas de forma diagonal descendente.
huecoAbajoDerecha(L,[[X,Y]|Tail]) :- tablero(X,Y,P) & tablero(X+1,Y+1,0) & tablero(X+2,Y+2,P) & huecoAbajoDerecha(LF,Tail) & .concat([[X+1,Y+1]],LF,L).
huecoAbajoDerecha(L,[[X,Y]|Tail]) :- huecoAbajoDerecha(L,Tail).
huecoAbajoDerecha([],[]).

// True si el jugador actual es due�o de la casilla
duenoCasilla(X,Y) :- .my_name(Agent) & (Agent == player1 | Agent == player) & tablero(X,Y,D) & D = 1.
duenoCasilla(X,Y) :- .my_name(Agent) & (Agent == player2) & tablero(X,Y,D) & D = 2.

/**
* Calcula la distancia entre dos puntos dados con la f�rmula D=?((X2-X1)^2+(Y2-Y1)^2)) 
* X1: numero de columna del punto 1.
* Y1: numero de fila del punto 1.
* X2: numero de columna del punto 2.
* Y1: numero de fila del punto 2.
*/
distancia(X1,Y1,X2,Y2,D) :- D = math.sqrt(((X2-X1)*(X2-X1))+((Y2-Y1)*(Y2-Y1))).

/**
* Genera una lista con todas las casillas del tablero que est�n ocupadas
* C: lista de estructura [[X1,Y1],[X2,Y2]�[XN,YN]] que contiene todas las casillas ocupadas del tablero.
*/
listarCasillasOcupadas(C) :- .findall([X,Y],tablero(X,Y,1),C1) & .findall([X,Y],tablero(X,Y,2),C2) & .concat(C1,C2,C).

/**
* Escoge una posici�n aleatoria de una lista dada que no haga cuatro en raya
* L: lista de estructura [[X1,Y1],[X2,Y2]�[XN,YN]] con todas las posiciones disponibles para escoger.
* X: numero de columna de la casilla escogida.
* Y: numero de fila de la casilla escogida.
*/
jugadaAleatoriaCorrecta(L,X,Y) :- 
	listarFichasColocadas(C) &
	perderPartida(R,C) &
	.difference(L,R,P) &
	escogerMovimiento(P,X,Y).

/**
* Genera una lista con la distancia de cada casilla de una lista dada a una casilla de referencia
* Ref: punto de referencia de forma [X1,Y2]
* @param2: lista de estructura [[X1,Y1],[X2,Y2]�[XN,YN]] con los puntos desde los que se quiere calcular la distancia con Ref.
* L: lista de estructura [[X1,Y1,D1],[X2,Y2,D2]�[XN,YN,DN]] donde se va concatenando en cada iteraci�n el punto y su distancia a Ref.
* Res: lista de estructura [[X1,Y1,D1],[X2,Y2,D2]�[XN,YN,DN]] que contiene la distancia de todos los elementos de param2
*/
listaDistancias(Ref,[],L,Res) :- L = Res.
listaDistancias([X1,Y1],[[X2,Y2]|Tail],L,Res) :- 
	distancia(X1,Y1,X2,Y2,D) &
	.concat(L,[[X1,Y1,D]],NL) &
	listaDistancias([X1,Y1],Tail,NL,Res).
	
//(Lista[X,Y,Distancia],Auxiliar,Resultado)Devuelve la casilla m�s lejana dada una lista de casillas de la forma [X,Y,Distancia]
/**
Dada una lista de coordenadas y distancias a una casilla de referencias, calcula cual de ellas est� a una mayor distancia.
* @param1: lista de estructura [[X1,Y1,D1],[X2,Y2,D2]�[XN,YN,DN]]
*/
maxDistancia([],Temp,Res) :- 
	Res = Temp.
maxDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :- 
	D > DT & 
	listarFichasColocadas(C) &
	maxDistancia(Tail,[X,Y,D],Res).
maxDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :- 
	maxDistancia(Tail,[XT,YT,DT],Res).

//(Lista[X,Y,Distancia],Auxiliar,Resultado)Devuelve la casilla m�s cercana dada una lista de casillas de la forma [X,Y,Distancia]
minDistancia([],Temp,Res) :- 
	Res = Temp.
minDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :- 
	D < DT & 
	listarFichasColocadas(C) &
	minDistancia(Tail,[X,Y,D],Res).
minDistancia([[X,Y,D]|Tail],[XT,YT,DT],Res) :- 
	minDistancia(Tail,[XT,YT,DT],Res).

//Genera una lista con todas las casillas vac�as y a cuanto est� su casilla ocupada m�s cercana
posiblesJugadasPerder([],O,Temp,Res) :- Temp = Res.
posiblesJugadasPerder([[X,Y]|Tail],O,Temp,Res) :- 
	listaDistancias([X,Y],O,[],LD) &
	posicionLibre(L) &
	jugadaAleatoriaCorrecta(L,X1,Y1) &
	minDistancia(LD,[X1,Y1,10],MD) &
	.concat(Temp,[MD],TempRes) &
	posiblesJugadasPerder(Tail,O,TempRes,Res).
	
//Escoge la casilla vac�a cuya casilla ocupada m�s cercana est� m�s lejos
casillaOptimaPerder([],O,X,Y) :- posicionLibre(L) & escogerMovimiento(L,X,Y).
casillaOptimaPerder(L,O,XT,YT) :- 
	posiblesJugadasPerder(L,O,[],Res) &
	jugadaAleatoriaCorrecta(L,X1,Y1) &
	maxDistancia(Res,[X1,Y1,0],[XT,YT,DT]).

//Escoge la pr�xima jugada intentando que sea el rival el que gane
escogerMovimientoPerder(0,0) :- tableroVacio.
escogerMovimientoPerder(X,Y) :- 
	posicionLibre(L) & 
	.print("Libres: ",L) &
	listarFichasColocadas(O) &
	perderPartida(Perder,O) &
	.print("Perder: ",Perder) &
	.difference(L,Perder,L2) &
	casillaOptimaPerder(L2,O,X,Y).

+turno(Player)[source(percept)]: jugadorActual(Player) & estrategia(Estrategia)[source(percept)] & Estrategia = jugarAGanar <-
	.print("TURNO: ",Player);
	.wait(500);
	!escogerMovimientoPerder.

+turno(Player)[source(percept)]: jugadorActual(Player) & estrategia(Estrategia)[source(percept)] & Estrategia = jugarAPerder <-
	.print("TURNO: ",Player);
	.wait(500);
	!escogerMovimientoPerder.
	
+turno(Player).


+!escogerMovimientoGanar: listarMovimientos(L) & escogerMovimiento(L,X,Y) <-
	.print(X," ",Y);
	put(X,Y).
	
+!escogerMovimientoGanar <-
	.print("MOVIMIENTO INVALIDO").

+!escogerMovimientoPerder: escogerMovimientoPerder(X,Y) /*& movimientoValido(X,Y)*/ <- 
	.print(X," ",Y);
	put(X,Y).
	
+!escogerMovimientoPerder <-
	.print("MOVIMIENTO INVALIDO"). 


